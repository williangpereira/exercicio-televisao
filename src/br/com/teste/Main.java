package br.com.teste;

public class Main {

	public static void main(String[] args) {	

		Televisao televisao = new Televisao(5,5);
		
		ControleRemoto controle = new ControleRemoto(televisao);
		
		controle.aumentaCanal();
		controle.diminuiCanal();
		controle.aumentaCanalIndicado();
		controle.getVolume();
		}
		
	

}
